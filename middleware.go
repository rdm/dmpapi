package main

import (
	"context"
	"dmpapi/db"
	s "dmpapi/settings"
	t "dmpapi/transport"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	"net/http"
)

func parameterMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		params, err := t.InitParams(r)
		if err != nil {
			t.ErrorResponse(w, fmt.Errorf("invalid query parameters: %w", err), http.StatusBadRequest)
		}
		ctx := context.WithValue(r.Context(), s.PARAMETER_KEY, params)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func contentMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}

func loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Info().Msgf("%s: %s", r.Method, r.RequestURI)
		next.ServeHTTP(w, r)
	})
}

func checkCrendetials(r *http.Request) error {
	username, _, _ := r.BasicAuth()
	if username != s.SUPER_USER {
		// check that we got the credentials for the requested data project
		projectId := mux.Vars(r)["projectId"]
		if projectId != username[len(username)-len(projectId):] {
			return fmt.Errorf("insufficient permission to access data project %v", projectId)
		}
	}
	return nil
}

type getFunc func(t.QueryParam) (db.DBData, error)

func getWrapper(db *db.DataBase, reader getFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if err := checkCrendetials(r); err != nil {
			t.ErrorResponse(w, err, http.StatusUnauthorized)
			return
		}
		// open DB connection
		username, password, _ := r.BasicAuth()
		if err := db.Connect(username, password); err != nil {
			t.ErrorResponse(w, err, http.StatusUnauthorized)
			return
		}
		defer db.Close()

		p, err := t.InitParams(r)
		if err != nil {
			t.ErrorResponse(w, fmt.Errorf("invalid query parameters: %w", err), http.StatusBadRequest)
			return
		}
		data, err := reader(p)

		if err != nil {
			t.ErrorResponse(w, fmt.Errorf("failed to retrieve data: %w", err), http.StatusInternalServerError)
			return
		}

		t.GetResponse(w, p.GetPages(r.URL, data.Length()), data)
	})
}

type postFunc func(w http.ResponseWriter, r *http.Request, p t.QueryParam) (*http.Request, error)

func postWrapper(db *db.DataBase, reader postFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if err := checkCrendetials(r); err != nil {
			t.ErrorResponse(w, err, http.StatusUnauthorized)
			return
		}

		// open DB connection
		username, password, _ := r.BasicAuth()
		if err := db.Connect(username, password); err != nil {
			t.ErrorResponse(w, err, http.StatusUnauthorized)
			return
		}
		defer db.Close()

		p, err := t.InitParams(r)
		if err != nil {
			t.ErrorResponse(w, fmt.Errorf("invalid query parameters: %w", err), http.StatusBadRequest)
			return
		}

		req, err := reader(w, r, p)
		if err != nil {
			t.ErrorResponse(w, fmt.Errorf("failed to post data: %w", err), http.StatusInternalServerError)
			return
		}

		if err := t.MakeRequest(req); err != nil {
			t.ErrorResponse(w, fmt.Errorf("request failed: %w", err), http.StatusBadRequest)
			return
		}

		t.PostResponse(w)
	})
}
