An RESTful API to the logger component of the UFZ-DMP

# Status
This is work in progress and should definitely not be exposed to the wild!

# Local Usage

## Build the docker image
```sh
docker build -t dmpapi:latest . -f Dockerfile
```

Depending on your system, it might be necessary to tell docker to use the same network as the host:
```sh
docker build --network=host -t dmpapi:latest . -f Dockerfile
```

## Run the docker image
```sh
docker run --rm --publish 8080:8080 dmpapi:latest
```

or as a daemon process
```sh
docker run --detach --network=host --restart unless-stopped --env APIUSER={USERNAME} --env APIPASS={PASSWORD} dmpapi:latest
```

## Prebuilt images
We also provide prebuilt images [here](https://git.ufz.de/rdm/dmpapi/container_registry).
To run the API as a sytstem wide service, something like that should be the fastest way, to get things up and runinng:
```sh
docker run --detach --network=host --restart unless-stopped --env APIUSER={USERNAME} --env APIPASS={PASSWORD} git.ufz.de:4567/rdm/dmpapi:0.10.1
```


## Acces
If everything goes according to plan, the API should be available under
```
http://localhost:8888/v1/projects/{projectId}/...
```

E.g.
```
curl -u {username}:{password} --request GET http://localhost:8888/v1/projects/{projectId}/loggers
```
where `username` and `password` denote the credentials for the respective data project.
