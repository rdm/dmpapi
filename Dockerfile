FROM golang:1.21-bookworm AS builder

WORKDIR /build

# dependency management
COPY go.mod .
COPY go.sum .

# copy the source files
COPY controller ./controller
COPY db ./db
COPY transport ./transport
COPY settings ./settings
COPY *.go ./

RUN go build -o dmpapi .

FROM debian:bookworm-slim
RUN apt-get update && apt-get install -y libaio1 zip curl

## get the instant client

ARG release=19
ARG update=6

RUN curl -O https://download.oracle.com/otn_software/linux/instantclient/${release}${update}00/instantclient-basic-linux.x64-${release}.${update}.0.0.0dbru.zip

## install the instant client
RUN unzip instantclient-basic-linux.x64-${release}.${update}.0.0.0dbru.zip && \
    mv instantclient_${release}_${update} /usr/lib/ && \
    echo /usr/lib/instantclient_${release}_${update} > /etc/ld.so.conf.d/oracle-instantclient.conf && \
    ldconfig

## setup the instant client
ENV LD_LIBRARY_PATH /usr/lib/instantclient_${release}_${update}
COPY wallet/* /usr/lib/instantclient_${release}_${update}/network/admin/

WORKDIR /dmpapi

COPY --from=builder /build/dmpapi .

EXPOSE 8080

CMD ["./dmpapi"]
