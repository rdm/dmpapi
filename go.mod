module dmpapi

go 1.21

toolchain go1.21.4

require (
	github.com/godror/godror v0.40.4
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/schema v1.2.0
	github.com/jmoiron/sqlx v1.3.1
	github.com/rs/zerolog v1.20.0
)

require (
	github.com/go-logfmt/logfmt v0.6.0 // indirect
	github.com/godror/knownpb v0.1.1 // indirect
	github.com/sijms/go-ora/v2 v2.7.26 // indirect
	golang.org/x/exp v0.0.0-20231206192017-f3f8817b8deb // indirect
	golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
)
