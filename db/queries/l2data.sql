-- l2data.sql
SELECT
  TO_CHAR(r.timestamp_measurement, 'YYYY-MM-DD HH24:MI:SS') AS timestamp_measurement
  , v.value AS value_measurement
  , l.logger_id
  , s.sensor_id
  , s.label AS sensor_label
  , s.name AS sensor_name
  , f.quality_flag
  , f.quality_cause
  , f.quality_comment
  FROM LOGGER.LOGGER l
         JOIN LOGGER.LEVEL1_RECORD r   ON l.logger_id        = r.logger_id
         JOIN LOGGER.LEVEL1_VALUE  v   ON r.level1_record_id = v.level1_record_id
         JOIN LOGGER.SENSOR        s   ON v.sensor_id        = s.sensor_id
         LEFT OUTER JOIN LOGGER.FLAG f on v.level1_value_id  = f.level1_value_id
 WHERE l.datenprojekt_id = :1
   AND %s = :2
   AND r.timestamp_measurement >= LOGGER.DMP_DATE.PARSE(:3)
	 AND r.timestamp_measurement <= LOGGER.DMP_DATE.PARSE(:4)
   AND s.evaluate_sensor = 1     -- exclude the timestamp only sensors
   AND l.deleted = 0             -- leave the dead alone
   AND s.deleted = 0
 ORDER BY r.timestamp_measurement, s.sensor_id
          OFFSET :5 ROWS FETCH NEXT :6 ROWS ONLY
