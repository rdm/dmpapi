SELECT
  s.name AS sensor_name
  , s.label AS sensor_label
  , s.position AS sensor_position
  , CASE WHEN c.config_start IS NULL THEN '%s' ELSE TO_CHAR(c.config_start, 'YYYY-MM-DD HH24:MI:SS') END AS start_date
  , CASE WHEN c.config_end IS NULL THEN '%s' ELSE TO_CHAR(c.config_end, 'YYYY-MM-DD HH24:MI:SS') END AS end_date
  , CASE WHEN s.sampling_media_name = 'TIME' AND s.evaluate_sensor = 0 THEN 1 ELSE 0 END AS is_time
  , c.LABEL AS configuration_label
  , l.type AS logger_type
  FROM
      logger.sensor s
      JOIN logger.sensor_configuration c ON c.sensor_configuration_id = s.sensor_configuration_id
      JOIN logger.logger               l on l.logger_id               = c.logger_id
 WHERE l.datenprojekt_id = :1
   AND l.logger_id = :2
   AND s.updated = 0             -- the generic rewrite parser only uses the latest setup
   AND s.virtual_sensor = 0      -- we don't want virtual sensors, as they don't accept upload data
   AND l.deleted = 0             -- leave the dead alone
   AND s.deleted = 0
   AND c.deleted = 0
   AND c.bypass = 0              -- only valid sensor configurations
