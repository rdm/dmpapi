WITH tmp AS (
  SELECT
    TO_CHAR(TRUNC(r.timestamp_measurement, '%s'), 'YYYY-MM-DD HH24:MI:SS') AS timestamp_measurement
    , CASE WHEN REGEXP_LIKE(v.value, '^[\+-]?\d+(\.\d+)?$') THEN TO_NUMBER(v.VALUE) ELSE NULL END AS value_measurement
    , l.logger_id
    , s.sensor_id
    , s.label AS sensor_label
    , s.name AS sensor_name
    FROM LOGGER.LOGGER l
           JOIN LOGGER.LEVEL1_RECORD r ON l.logger_id        = r.logger_id
           JOIN LOGGER.LEVEL1_VALUE  v ON r.level1_record_id = v.level1_record_id
           JOIN LOGGER.SENSOR        s ON v.sensor_id        = s.sensor_id
   WHERE l.datenprojekt_id = :1
     AND %s = :2
     AND r.timestamp_measurement >= LOGGER.DMP_DATE.PARSE(:3)
		 AND r.timestamp_measurement <= LOGGER.DMP_DATE.PARSE(:4)
     AND s.evaluate_sensor = 1     -- exclude the timestamp only sensors
     AND l.deleted = 0             -- leave the dead alone
     AND s.deleted = 0
) SELECT
    timestamp_measurement,
    %s(value_measurement) AS value_measurement,
    MAX(logger_id) AS logger_id,
    sensor_id,
    MAX(sensor_name) AS sensor_name,
    MAX(sensor_label) AS sensor_label
    FROM tmp
   GROUP BY timestamp_measurement, sensor_id
   ORDER BY timestamp_measurement, logger_id, sensor_id
            OFFSET :5 ROWS FETCH NEXT :6 ROWS ONLY
