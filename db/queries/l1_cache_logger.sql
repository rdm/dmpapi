-- l1_cache_logger.sql
SELECT
  TO_CHAR(d.timestamp, 'YYYY-MM-DD HH24:MI:SS') AS timestamp_measurement
  , d.value AS value_measurement
  , d.logger_id
  , d.sensor_id
  , '' AS sensor_label
  , '' AS sensor_name
  FROM LOGGER.V_LEVEL1_DATA d
 WHERE
   d.sensor_id IN (
     SELECT
       s.sensor_id
       FROM LOGGER.SENSOR s
              JOIN LOGGER.SENSOR_CONFIGURATION c on s.sensor_configuration_id = c.sensor_configuration_id
              JOIN LOGGER.LOGGER l               on l.logger_id               = c.logger_id
      WHERE l.logger_id = :1
        %s
        AND s.evaluate_sensor = 1     -- exclude the timestamp only sensors
        AND l.deleted = 0             -- leave the dead alone
        AND s.deleted = 0
        AND c.deleted = 0
   )
   AND d.timestamp >= LOGGER.DMP_DATE.PARSE(:2)
	 AND d.timestamp <= LOGGER.DMP_DATE.PARSE(:3)
   OFFSET :4 ROWS FETCH NEXT :5 ROWS ONLY
