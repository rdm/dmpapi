-- loggers.sql
SELECT
  logger_id,
  device_id,
  name AS logger_name,
  label AS logger_label,
  responsible_person,
  untersuchungsgebiet_id
  FROM
      LOGGER.LOGGER l
 WHERE l.datenprojekt_id = :1
       %s
 ORDER BY l.logger_id
          OFFSET :2 ROWS FETCH NEXT :3 ROWS ONLY

