-- value_ids.sql
SELECT
  TO_CHAR(r.timestamp_measurement, 'YYYY-MM-DD HH24:MI:SS') AS timestamp_measurement
  , s.sensor_id
  , s.name AS sensor_name
  , s.label AS sensor_label
  , v.level1_value_id AS value_id
  FROM LOGGER.LOGGER l
         JOIN LOGGER.LEVEL1_RECORD        r ON l.LOGGER_ID               = r.LOGGER_ID
         JOIN LOGGER.LEVEL1_VALUE         v ON r.LEVEL1_RECORD_ID        = v.LEVEL1_RECORD_ID
         JOIN LOGGER.SENSOR               s ON v.SENSOR_ID               = s.SENSOR_ID
         JOIN LOGGER.SENSOR_CONFIGURATION c on s.sensor_configuration_id = c.sensor_configuration_id
 WHERE l.datenprojekt_id = :1
   AND l.logger_id = :2
   AND timestamp_measurement >= LOGGER.DMP_DATE.PARSE(:3)
	 AND timestamp_measurement <= LOGGER.DMP_DATE.PARSE(:4)
   -- AND s.virtual_sensor = 0      -- we don't want virtual sensors, as they don't accept upload data
   AND s.evaluate_sensor = 1     -- exclude the timestamp only sensors
   AND l.deleted = 0             -- leave the dead alone
   AND s.deleted = 0
   AND c.deleted = 0
 ORDER BY s.sensor_id, r.timestamp_measurement
