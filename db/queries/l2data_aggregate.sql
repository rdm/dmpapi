 -- l2data_aggregate.sql
WITH selected AS (
	SELECT
    TO_CHAR(TRUNC(r.timestamp_measurement, '%s'), 'YYYY-MM-DD HH24:MI:SS') AS timestamp_measurement
    , CASE WHEN REGEXP_LIKE(v.value, '^[\+-]?\d+(\.\d+)?$') THEN TO_NUMBER(v.value) ELSE NULL END AS value_measurement
    , l.logger_id
    , s.sensor_id
    , s.label AS sensor_label
    , s.name AS sensor_name
    , f.quality_flag
    , f.quality_cause
    , f.quality_comment
    FROM LOGGER.LOGGER l
           JOIN LOGGER.LEVEL1_RECORD   r ON l.logger_id        = r.logger_id
           JOIN LOGGER.LEVEL1_VALUE    v ON r.level1_record_id = v.level1_record_id
           JOIN LOGGER.SENSOR          s ON v.sensor_id        = s.sensor_id
           LEFT OUTER JOIN LOGGER.FLAG f on v.level1_value_id  = f.level1_value_id
   WHERE l.datenprojekt_id = :1
     AND %s = :2
     AND r.timestamp_measurement >= LOGGER.DMP_DATE.PARSE(:3)
	   AND r.timestamp_measurement <= LOGGER.DMP_DATE.PARSE(:4)
     AND s.evaluate_sensor = 1     -- exclude the timestamp only sensors
     AND l.deleted = 0             -- leave the dead alone
     AND s.deleted = 0
) SELECT
	  timestamp_measurement AS timestamp_measurement,
	  MAX(logger_id) AS logger_id,
	  %s(CASE WHEN quality_flag in ('BAD', 'DOUBTFUL') THEN NULL ELSE value_measurement END) AS value_measurement,
	  MAX(sensor_id) AS sensor_id,
	  MAX(sensor_name) AS sensor_name,
	  MAX(sensor_label) AS sensor_label,
	  CASE
	  WHEN AVG(CASE WHEN quality_flag = 'BAD' THEN 1 ELSE 0 END) > :5
      THEN 'BAD'
	  WHEN AVG(CASE WHEN quality_flag in ('BAD', 'DOUBTFUL') THEN 1 ELSE 0 END) > :6
      THEN 'DOUBTFUL'
	  ELSE 'OK'
	  END AS quality_flag,
	  NULL AS quality_cause,
	  NULL AS quality_comment
	  FROM selected
	 GROUP BY timestamp_measurement, sensor_id
	 ORDER BY timestamp_measurement, sensor_id
	          OFFSET :7 ROWS FETCH NEXT :8 ROWS ONLY
