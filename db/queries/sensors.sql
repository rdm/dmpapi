-- sensors.sql
SELECT
  s.sensor_id
  , s.name AS sensor_name
  , s.label AS sensor_label
  , s.compartment_name
  , s.physical_property_name
  , s.updated
  , s.deleted
  , s.virtual_sensor AS virtual
  , s.unit_name
  , s.sampling_interval
  , s.sampling_altitude -- elevation of the sensor position
  , c.logger_id
  , o.hoehe_nn          -- elevation of the sampling location
  , o.lat
  , o.lon
  , TO_CHAR(c.config_start, 'YYYY-MM-DD HH24:MI:SS') AS start_date
  , TO_CHAR(c.config_end, 'YYYY-MM-DD HH24:MI:SS') AS end_date
  FROM LOGGER.SENSOR s
         JOIN LOGGER.SENSOR_CONFIGURATION c on s.sensor_configuration_id = c.sensor_configuration_id
         JOIN LOGGER.LOGGER l               on l.logger_id               = c.logger_id
         JOIN DATENMANAGEMENTPORTAL.ORT o   on o.ort_id                  = s.ort_id
 WHERE l.datenprojekt_id = :1
   AND s.evaluate_sensor = 1     -- exclude the timestamp only sensors
   AND l.deleted = 0             -- leave the dead alone
   AND s.deleted = 0
   AND c.deleted = 0
       %s
       OFFSET :2 ROWS FETCH NEXT :3 ROWS ONLY
