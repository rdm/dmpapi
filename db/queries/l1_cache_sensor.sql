-- l1_cache_sensor.sql
SELECT
  TO_CHAR(d.timestamp, 'YYYY-MM-DD HH24:MI:SS') AS timestamp_measurement
  , d.value AS value_measurement
  , d.logger_id
  , d.sensor_id
  , '' AS sensor_label
  , '' AS sensor_name
  FROM LOGGER.V_LEVEL1_DATA d
 WHERE d.sensor_id = :1
   AND d.timestamp >= LOGGER.DMP_DATE.PARSE(:2)
	 AND d.timestamp <= LOGGER.DMP_DATE.PARSE(:3)
       OFFSET :4 ROWS FETCH NEXT :5 ROWS ONLY
