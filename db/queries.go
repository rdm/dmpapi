package db

import (
	s "dmpapi/settings"
	"embed"
	"fmt"
)

func readQuery(fs embed.FS, fname string) string {

	content, err := fs.ReadFile(fname)
	if err != nil {
		panic(fmt.Errorf("failed to read query from file '%v' (%v)", fname, err))
	}
	return string(content)
}

//go:embed queries/*
var fs embed.FS

var (
	MIN_DATE = s.MIN_DATE.Format(s.DATE_FORMAT)
	MAX_DATE = s.MAX_DATE.Format(s.DATE_FORMAT)

	// TODO: unify naming scheme
	VALUE_ID_QUERY = readQuery(fs, "queries/value_ids.sql")

	SENSORS_POSITION_QUERY      = fmt.Sprintf(readQuery(fs, "queries/sensors_positions.sql"), MIN_DATE, MAX_DATE)
	SENSORS_BASE_QUERY          = readQuery(fs, "queries/sensors.sql")
	SENSORS_QUERY               = fmt.Sprintf(SENSORS_BASE_QUERY, "")
	SENSORS_BY_SENSORNAME_QUERY = fmt.Sprintf(SENSORS_BASE_QUERY, "AND s.NAME = :4")
	SENSORS_BY_SENSORID_QUERY   = fmt.Sprintf(SENSORS_BASE_QUERY, "AND s.SENSOR_ID = :4")
	SENSORS_BY_LOGGERID_QUERY   = fmt.Sprintf(SENSORS_BASE_QUERY, "AND c.LOGGER_ID = :4")

	LOGGERS_BASE_QUERY        = readQuery(fs, "queries/loggers.sql")
	LOGGERS_QUERY             = fmt.Sprintf(LOGGERS_BASE_QUERY, "")
	LOGGERS_BY_LOGGERID_QUERY = fmt.Sprintf(LOGGERS_BASE_QUERY, "AND l.LOGGER_ID = :4")

	L1_BASE_QUERY   = readQuery(fs, "queries/l1data.sql")
	L1_SENSOR_QUERY = fmt.Sprintf(L1_BASE_QUERY, "s.SENSOR_ID")
	L1_LOGGER_QUERY = fmt.Sprintf(L1_BASE_QUERY, "l.LOGGER_ID")

	L1_CACHE_SENSOR_QUERY = readQuery(fs, "queries/l1_cache_sensor.sql")
	L1_CACHE_LOGGER_QUERY = readQuery(fs, "queries/l1_cache_logger.sql")

	L1_AGGREGATE_BASE_QUERY   = readQuery(fs, "queries/l1data_aggregate.sql")
	L1_AGGREGATE_SENSOR_QUERY = fmt.Sprintf(L1_AGGREGATE_BASE_QUERY, "%s", "s.SENSOR_ID", "%s")
	L1_AGGREGATE_LOGGER_QUERY = fmt.Sprintf(L1_AGGREGATE_BASE_QUERY, "%s", "l.LOGGER_ID", "%s")

	L2_BASE_QUERY   = readQuery(fs, "queries/l2data.sql")
	L2_LOGGER_QUERY = fmt.Sprintf(L2_BASE_QUERY, "l.LOGGER_ID")
	L2_SENSOR_QUERY = fmt.Sprintf(L2_BASE_QUERY, "s.SENSOR_ID")

	L2_CACHE_SENSOR_QUERY = readQuery(fs, "queries/l2_cache_sensor.sql")
	L2_CACHE_LOGGER_QUERY = readQuery(fs, "queries/l2_cache_logger.sql")

	L2_AGGREGATE_BASE_QUERY   = readQuery(fs, "queries/l2data_aggregate.sql")
	L2_AGGREGATE_SENSOR_QUERY = fmt.Sprintf(L2_AGGREGATE_BASE_QUERY, "%s", "s.SENSOR_ID", "%s")
	L2_AGGREGATE_LOGGER_QUERY = fmt.Sprintf(L2_AGGREGATE_BASE_QUERY, "%s", "l.LOGGER_ID", "%s")
)
