package db

import (
	s "dmpapi/settings"
	t "dmpapi/transport"
	"encoding/json"
	"fmt"
	"github.com/rs/zerolog/log"
	"net/http"
	"net/url"
	"strings"
)

var (
	LOGGER_TYPES = map[string]bool{
		"GENERIC_UNIX":    true,
		"GENERIC_REWRITE": true,
	}
)

type Value struct {
	Date        UploadDate  `json:"timestamp"`
	Value       NullFloat64 `json:"value"`
	SensorLabel string      `json:"sensorLabel"`
	SensorName  string      `json:"sensorName"`
}

type SensorConfiguration struct {
	SensorName         string     `db:"SENSOR_NAME"`
	SensorLabel        string     `db:"SENSOR_LABEL"`
	Position           int        `db:"SENSOR_POSITION"`
	IsTime             Boolean    `db:"IS_TIME"`
	StartDate          UploadDate `db:"START_DATE"`
	EndDate            UploadDate `db:"END_DATE"`
	ConfigurationLabel string     `db:"CONFIGURATION_LABEL"`
	LoggerType         string     `db:"LOGGER_TYPE"`
}

type SensorConfigurations []SensorConfiguration

func (db *DataBase) querySensorPositions(projectId, loggerId int) (*SensorConfigurations, error) {

	out := SensorConfigurations{}
	if err := db.Select(&out, SENSORS_POSITION_QUERY, projectId, loggerId); err != nil {
		return nil, err
	}

	// a bunch of validity tests
	configs := map[string]int{}
	for _, c := range out {
		// only certain logger types are supported
		if _, found := LOGGER_TYPES[c.LoggerType]; !found {
			keys := []string{}
			for k, _ := range LOGGER_TYPES {
				keys = append(keys, k)
			}
			return nil, fmt.Errorf("unsupported logger type, expected one of: {%v}", strings.Join(keys, ", "))
		}

		// every sensor_configuration needs a non-evaluated timestamp sensor at position 0
		if _, ok := configs[c.ConfigurationLabel]; !ok {
			configs[c.ConfigurationLabel] = 0
		}
		if c.IsTime == true && c.Position == 0 {
			configs[c.ConfigurationLabel] += 1
		}
	}

	for k, v := range configs {
		if v < 1 {
			return nil, fmt.Errorf("sensor configuration '%v' is missing a (virtual) timestamp sensor at position 0", k)
		}
	}
	return &out, nil
}

func (positions SensorConfigurations) SensorCount() int {

	out := 0
	for _, pos := range positions {
		if pos.Position > out {
			out = pos.Position
		}
	}
	return out + 1
}

func (positions SensorConfigurations) ValuePosition(value Value) (int, error) {
	date := value.Date
	for _, pos := range positions {

		startDate := pos.StartDate
		endDate := pos.EndDate
		if (pos.SensorName == value.SensorName) || (pos.SensorLabel == value.SensorLabel) {
			if (startDate.Equal(date) || startDate.Before(date)) && (endDate.Equal(date) || endDate.After(date)) {
				return pos.Position, nil
			}
		}
	}
	return -1, fmt.Errorf(
		"no position found for sensor with name '%v' and label '%v' for %v", value.SensorName, value.SensorLabel, value.Date.Format(),
	)
}

type Values []Value

func (values Values) Encode(positions *SensorConfigurations) (string, error) {

	colCount := positions.SensorCount()

	header := make([]string, colCount)
	for _, pos := range *positions {
		header[pos.Position] = pos.SensorName
	}

	table := map[string][]string{}
	for _, value := range values {

		date := value.Date.Format()

		// did we already see this date?
		row, exists := table[date]
		if !exists {
			// no -> add a new row
			row = make([]string, colCount)
			row[0] = date
			table[date] = row
		}

		// write into the appropiate position
		pos, err := positions.ValuePosition(value)
		if err != nil {
			return "", err
		}
		if value.Value.Valid {
			row[pos] = fmt.Sprintf("%.12f", value.Value.Float64)
		}
	}

	rows := []string{strings.Join(header, ",")}
	for _, row := range table {
		rows = append(rows, strings.Join(row, ","))
	}
	// to ensure a empty line at file end as the DMP ignores thw last line of a file
	rows = append(rows, "")
	out := strings.Join(rows, "\n")
	log.Debug().Msg(out)
	return out, nil
}

func (db *DataBase) PostLevel1Data(dec *json.Decoder, projectId, loggerId int) (*http.Request, error) {

	data := Values{}

	if err := dec.Decode(&data); err != nil {
		return nil, fmt.Errorf("decoding error: %v", err)
	}

	sensorPositions, err := db.querySensorPositions(projectId, loggerId)
	if err != nil {
		return nil, err
	}

	deviceID, err := db.queryDeviceID(projectId, loggerId)
	if err != nil {
		return nil, err
	}

	uploadURL, _ := url.Parse(s.LEVEL1_URL + deviceID)

	content, err := data.Encode(sensorPositions)
	if err != nil {
		return nil, err
	}

	req, err := t.FileRequest(uploadURL, []byte(content))
	if err != nil {
		return nil, err
	}

	return req, nil
}
