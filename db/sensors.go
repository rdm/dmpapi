package db

import (
	"fmt"
	"math"
)

type locationModel struct {
	Lon    NullFloat64 `db:"LON"      json:"lon"`
	Lat    NullFloat64 `db:"LAT"      json:"lat"`
	Height NullFloat64 `db:"HOEHE_NN" json:"height"`
}

type sensorModel struct {
	ID        int     `db:"SENSOR_ID"    json:"sensorId"`
	Name      string  `db:"SENSOR_NAME"  json:"sensorName"`
	Label     string  `db:"SENSOR_LABEL" json:"sensorLabel"`
	LoggerID  int     `db:"LOGGER_ID"    json:"loggerId"`
	Updated   Boolean `db:"UPDATED"      json:"isUpdated"`
	Deleted   Boolean `db:"DELETED"      json:"isDeleted"`
	Virtual   Boolean `db:"VIRTUAL"      json:"isVirtual"`
	StartDate string  `db:"START_DATE"   json:"startDate"`
	EndDate   string  `db:"END_DATE"     json:"endDate"`
}

type variableModel struct {
	Variable    string `db:"PHYSICAL_PROPERTY_NAME" json:"variableName"`
	Compartment string `db:"COMPARTMENT_NAME"       json:"variableCompartment"`
}

type samplingModel struct {
	Unit     string  `db:"UNIT_NAME"         json:"samplingUnit"`
	Interval int     `db:"SAMPLING_INTERVAL" json:"samplingInterval"`
	Altitude float64 `db:"SAMPLING_ALTITUDE" json:"samplingHeight"`
}

type SensorModel struct {
	sensorModel
	variableModel
	samplingModel
	locationModel
}

type SensorData []SensorModel

func (d *SensorData) Length() int {
	return len(*d)
}

func (db *DataBase) querySensorInformation(projectId, loggerId int) (map[int]SensorModel, error) {

	// return sensor_ids and a id based lookup table into the query results

	sensors, err := db.GetSensorsByLoggerID(projectId, loggerId, 0, math.MaxInt64)
	if err != nil {
		return nil, err
	}
	if len(*sensors) == 0 {
		return nil, fmt.Errorf("no such logger found: %v", loggerId)
	}

	lut := map[int]SensorModel{}
	for _, row := range *sensors {
		lut[row.ID] = row
	}

	return lut, nil
}

func (db *DataBase) GetSensorsAll(projectId, start, limit int) (*SensorData, error) {
	return db.querySensors(
		SENSORS_QUERY,
		projectId,
		start, limit,
	)
}

func (db *DataBase) GetSensorsByID(projectId, sensorId, start, limit int) (*SensorData, error) {
	return db.querySensors(
		SENSORS_BY_SENSORID_QUERY,
		projectId, sensorId,
		start, limit,
	)
}

func (db *DataBase) GetSensorsByName(projectId int, name string, start, limit int) (*SensorData, error) {
	return db.querySensors(
		SENSORS_BY_SENSORNAME_QUERY,
		projectId, name,
		start, limit,
	)
}

func (db *DataBase) GetSensorsByLoggerID(projectId, loggerId, start, limit int) (*SensorData, error) {
	return db.querySensors(SENSORS_BY_LOGGERID_QUERY, projectId, loggerId, start, limit)
}

func (db *DataBase) querySensors(query string, args ...interface{}) (*SensorData, error) {

	rows, err := db.Queryx(query, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var out SensorData
	for rows.Next() {
		rowData := SensorModel{}
		err := rows.StructScan(&rowData)
		if err != nil {
			return nil, err
		}
		out = append(out, rowData)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return &out, nil
}
