package db

import (
	s "dmpapi/settings"
	"fmt"
	"math"
	"strings"
	"time"
)

type recordModel struct {
	Timestamp string `db:"TIMESTAMP_MEASUREMENT" json:"timestamp"`
	LoggerID  int    `db:"LOGGER_ID"             json:"loggerId"`
}

type dataModel struct {
	Value       NullFloat64 `db:"VALUE_MEASUREMENT" json:"value"`
	SensorID    int         `db:"SENSOR_ID"         json:"sensorId"`
	SensorName  string      `db:"SENSOR_NAME"       json:"sensorName"`
	SensorLabel string      `db:"SENSOR_LABEL"      json:"sensorLabel"`
}

type L2DataModel struct {
	recordModel
	dataModel
	Flag    string `db:"QUALITY_FLAG"    json:"qualityFlag"`
	Cause   string `db:"QUALITY_CAUSE"   json:"qualityCause"`
	Comment string `db:"QUALITY_COMMENT" json:"qualityComment"`
}

type L2Data []L2DataModel

func (d *L2Data) Length() int {
	return len(*d)
}

func (db *DataBase) GetLoggerDataLevel2Cached(projectId, loggerId, start, limit int, startDate, endDate time.Time) (*L2Data, error) {
	lut, err := db.querySensorInformation(projectId, loggerId)
	if err != nil {
		return nil, err
	}

	data, err := db.getL2Data(
		L2_CACHE_LOGGER_QUERY,
		loggerId,
		startDate.Format(s.DATE_FORMAT), endDate.Format(s.DATE_FORMAT),
		start, limit,
	)

	if err != nil {
		return nil, err
	}

	for i, row := range *data {
		sensor := lut[row.SensorID]
		row.SensorName = sensor.Name
		row.SensorLabel = sensor.Label
		(*data)[i] = row
	}

	return data, nil
}

func (db *DataBase) GetLoggerDataLevel2(projectId, loggerId, start, limit int, startDate, endDate time.Time) (*L2Data, error) {
	return db.getL2Data(
		L2_LOGGER_QUERY,
		projectId, loggerId,
		startDate.Format(s.DATE_FORMAT), endDate.Format(s.DATE_FORMAT),
		start, limit,
	)
}

func (db *DataBase) GetSensorDataLevel2Cached(projectId, sensorId, start, limit int, startDate, endDate time.Time) (*L2Data, error) {
	sensors, err := db.GetSensorsByID(projectId, sensorId, 0, math.MaxInt64)
	if err != nil {
		return nil, err
	}
	if len(*sensors) == 0 {
		return nil, fmt.Errorf("no such sensor found: %v", sensorId)
	}
	sensor := (*sensors)[0]

	data, err := db.getL2Data(
		L2_CACHE_SENSOR_QUERY,
		sensor.ID,
		startDate.Format(s.DATE_FORMAT), endDate.Format(s.DATE_FORMAT),
		start, limit,
	)
	if err != nil {
		return data, err
	}

	for i, row := range *data {
		row.SensorName = sensor.Name
		row.SensorLabel = sensor.Label
		(*data)[i] = row
	}
	return data, nil
}

func (db *DataBase) GetSensorDataLevel2(projectId, sensorId, start, limit int, startDate, endDate time.Time) (*L2Data, error) {
	return db.getL2Data(
		L2_SENSOR_QUERY,
		projectId, sensorId,
		startDate.Format(s.DATE_FORMAT), endDate.Format(s.DATE_FORMAT),
		start, limit,
	)
}

func (db *DataBase) GetSensorDataLevel2Aggregate(projectId, sensorId, start, limit int, startDate, endDate time.Time, badMax, doubtMax float64, aggregate, function string) (*L2Data, error) {
	query := fmt.Sprintf(L2_AGGREGATE_SENSOR_QUERY, s.AGGREGATION_PERIODS[aggregate], s.AGGREGATION_FUNCTIONS[function])
	return db.getL2Data(
		query,
		projectId, sensorId,
		startDate.Format(s.DATE_FORMAT), endDate.Format(s.DATE_FORMAT),
		badMax, doubtMax,
		start, limit,
	)
}

func (db *DataBase) GetLoggerDataLevel2Aggregate(projectId, loggerId, start, limit int, startDate, endDate time.Time, badMax, doubtMax float64, aggregate, function string) (*L2Data, error) {
	query := fmt.Sprintf(L2_AGGREGATE_LOGGER_QUERY, s.AGGREGATION_PERIODS[aggregate], s.AGGREGATION_FUNCTIONS[function])
	return db.getL2Data(
		query,
		projectId, loggerId,
		startDate.Format(s.DATE_FORMAT), endDate.Format(s.DATE_FORMAT),
		badMax, doubtMax,
		start, limit,
	)
}

func (db *DataBase) getL2Data(query string, args ...interface{}) (*L2Data, error) {

	rows, err := db.Queryx(query, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var out L2Data
	for rows.Next() {
		rowData := L2DataModel{}
		err := rows.StructScan(&rowData)
		if err != nil {
			return nil, err
		}
		rowData.Comment = strings.ReplaceAll(rowData.Comment, "\\", "")
		out = append(out, rowData)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return &out, nil
}
