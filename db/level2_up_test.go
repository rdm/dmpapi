package db

import (
	// "net/http/httptest"
	// "strings"
	"testing"
)

func TestCheckerFail(t *testing.T) {
	tests := []Flags{
		Flags{{ID: 1, Flag: "FAIL"}},
		Flags{{ID: 1, Cause: "FAIL"}},
		Flags{{ID: 1, Cause: "FAIL"}},
		Flags{{ID: 1, Flag: "BAD", Cause: ""}},
		Flags{{ID: 1, Flag: "DOUBTFUL", Cause: ""}},
		Flags{{ID: 1, Cause: "OTHER", Comment: ""}},
	}

	for _, test := range tests {
		err := checkFlags(test)
		if err == nil {
			t.Fatalf("flags checker did not fail on invalid input: %v", test)
		}
	}
}

func TestCheckerSuccess(t *testing.T) {
	tests := []Flags{
		Flags{{ID: 1, Flag: "OK"}},
		Flags{{ID: 1, Flag: "DOUBTFUL", Cause: "BELOW_MINIMUM"}},
		Flags{{ID: 1, Flag: "BAD", Cause: "ABOVE_MAXIMUM"}},
		Flags{{ID: 1, Flag: "BAD", Cause: "OTHER", Comment: "Just sayin"}},
	}

	for _, test := range tests {
		err := checkFlags(test)
		if err != nil {
			t.Fatalf("flags checker did fail on valid input: %v", test)
		}
	}
}

// func TestDecodingSucess(t *testing.T) {

// 	tests := []struct {
// 		body     string
// 		expected Flags
// 	}{
// 		{
// 			"[{\"level1_value_id\": 1, \"quality_flag\":\"OK\"}]",
// 			Flags{{ID: 1, Flag: "OK"}}},
// 		{
// 			"[{\"level1_value_id\": 1, \"quality_flag\":\"OK\", \"quality_comment\": \"Test\"}]",
// 			Flags{{ID: 1, Flag: "OK", Comment: "Test"}},
// 		},
// 		{
// 			"[{\"level1_value_id\": 1, \"quality_flag\":\"BAD\", \"quality_cause\": \"BATTERY_LOW\"}]",
// 			Flags{{ID: 1, Flag: "BAD", Cause: "BATTERY_LOW"}},
// 		},
// 	}

// 	for _, test := range tests {
// 		w := httptest.NewRecorder()
// 		r := httptest.NewRequest("POST", "http://my.dummy.com/flags", strings.NewReader(test.body))

// 		data, err := DecodeFlags(w, r)
// 		if err != nil {
// 			t.Fatalf("flags decoding failed on valid input %v", test.body)
// 		}
// 		if data.(Flags)[0] != test.expected[0] {
// 			t.Fatalf("flags decoding return %v but expected %v", data, test.expected)

// 		}
// 	}
// }

func TestDecodingFail(t *testing.T) {
	// TODO
}
