package db

import (
	s "dmpapi/settings"
	t "dmpapi/transport"
	"encoding/json"
	"fmt"
	"github.com/rs/zerolog/log"
	"net/http"
	"net/url"
	"strings"
	"time"
)

var (
	// a poor man's set
	FLAGS = map[string]bool{
		"OK":       true,
		"DOUBTFUL": true,
		"BAD":      true,
	}

	CAUSES = map[string]bool{
		"":                       true,
		"BATTERY_LOW":            true,
		"BELOW_MINIMUM":          true,
		"ABOVE_MAXIMUM":          true,
		"BELOW_OR_ABOVE_MIN_MAX": true,
		"ISOLATED_SPIKE":         true,
		"DEFECTIVE_SENSOR":       true,
		"LEFT_CENSORED_DATA":     true,
		"RIGHT_CENSORED_DATA":    true,
		"OTHER":                  true,
	}
)

type Flag struct {
	Timestamp   UploadDate `json:"timestamp"`
	SensorID    int        `json:"sensorId"`
	SensorLabel string     `json:"sensorLabel"`
	SensorName  string     `json:"sensorName"`
	Flag        string     `json:"qualityFlag"`
	Cause       string     `json:"qualityCause"`
	Comment     string     `json:"qualityComment"`
}

func (f *Flag) UnmarshalJSON(text []byte) error {
	// NOTE: type alias needed, to prevent infinite recursion
	type flag Flag
	flgs := flag{SensorID: -1}
	if err := json.Unmarshal(text, &flgs); err != nil {
		return err
	}
	*f = Flag(flgs)
	return nil
}

type Flags []Flag

func (flags Flags) DateRange() (UploadDate, UploadDate) {

	minDate := time.Time(flags[0].Timestamp)
	maxDate := time.Time(flags[0].Timestamp)

	for _, flag := range flags {
		thisDate := time.Time(flag.Timestamp)
		if thisDate.Before(minDate) {
			minDate = thisDate
		} else if thisDate.After(maxDate) {
			maxDate = thisDate
		}
	}
	return UploadDate(minDate), UploadDate(maxDate)
}

func (flags Flags) CheckFlags() error {
	for i, row := range flags {
		line := i + 2
		if _, ok := FLAGS[row.Flag]; !ok {
			return fmt.Errorf("line %v: invalid qualityFlag '%s'", line, row.Flag)
		}

		if _, ok := CAUSES[row.Cause]; !ok {
			return fmt.Errorf("line %v: invalid qualityCause: '%s'", line, row.Cause)
		}

		if (row.Flag != "OK") && (row.Cause == "") {
			return fmt.Errorf("line %v: qualityCause needed if quality_flag is not 'OK'", line)
		}

		if row.Cause == "OTHER" && (row.Comment == "") {
			return fmt.Errorf("line %v: qualityComment needed if quality_cause is set to 'OTHER'", line)
		}
	}
	return nil
}

type ValueModel struct {
	Timestamp   UploadDate `db:"TIMESTAMP_MEASUREMENT"`
	SensorId    int        `db:"SENSOR_ID"`
	SensorLabel string     `db:"SENSOR_LABEL"`
	SensorName  string     `db:"SENSOR_NAME"`
	ValueId     int        `db:"VALUE_ID"`
}

func (db *DataBase) queryValueID(projectId, loggerId int, startDate, endDate UploadDate) ([]ValueModel, error) {
	rows, err := db.Queryx(
		VALUE_ID_QUERY,
		projectId, loggerId,
		startDate.Format(), endDate.Format(),
	)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	out := []ValueModel{}
	for rows.Next() {
		rowData := ValueModel{}
		err := rows.StructScan(&rowData)
		if err != nil {
			return nil, err
		}
		out = append(out, rowData)
	}
	return out, nil
}

func (flags Flags) Encode(valueIds []ValueModel) (string, error) {

	rows := []string{"level1_value_id,quality_flag,quality_cause,quality_comment"}

	// NOTE:
	// we brute force the value_id mapping, there a certainly more elaborate
	// ways to do it, like a mergesort style merging on sorted slices, but as
	// it seems to be efficient enough like that, let's keep it simple
	for _, flag := range flags {

		valueID := -1
		for _, v := range valueIds {
			if v.Timestamp.Equal(flag.Timestamp) {
				if (flag.SensorID == v.SensorId) || (flag.SensorName == v.SensorName) || (flag.SensorLabel == v.SensorLabel) {
					valueID = v.ValueId
					// NOTE: escape commas in comment, as they are interpreted as column separator otherwise
					rowOut := []string{fmt.Sprintf("%v", valueID), flag.Flag, flag.Cause, jsonEscape(flag.Comment)}
					rows = append(rows, strings.Join(rowOut, ","))
				}
			}
		}
		if valueID == -1 {
			err := fmt.Errorf(
				"Invalid timestamp '%v' for sensor ('%v' - '%v' - '%v') ",
				flag.Timestamp.Format(), flag.SensorID, flag.SensorName, flag.SensorLabel,
			)
			return "", err
		}
	}
	out := strings.Join(rows, "\n")
	log.Debug().Msg(out)
	return out, nil
}

func jsonEscape(str string) string {
	json, err := json.Marshal(str)
	if err != nil {
		// NOTE: str is not a json string, so leave it alone
		return str
	}
	return string(json)
}

func (db *DataBase) PostLevel2Data(dec *json.Decoder, projectId, loggerId int) (*http.Request, error) {

	flags := Flags{}
	if err := dec.Decode(&flags); err != nil {
		return nil, err
	}

	if len(flags) == 0 {
		return nil, fmt.Errorf("empty request")
	}

	if err := flags.CheckFlags(); err != nil {
		return nil, err
	}

	deviceID, err := db.queryDeviceID(projectId, loggerId)
	if err != nil {
		return nil, err
	}

	minDate, maxDate := flags.DateRange()
	valueIDs, err := db.queryValueID(projectId, loggerId, minDate, maxDate)
	if err != nil {
		return nil, err
	}

	content, err := flags.Encode(valueIDs)
	if err != nil {
		return nil, err
	}

	uploadURL, _ := url.Parse(s.LEVEL2A_URL + deviceID)
	req, err := t.FileRequest(uploadURL, []byte(content))
	if err != nil {
		return nil, err
	}

	return req, nil
}
