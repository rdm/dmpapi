package db

import (
	"fmt"
)

type LoggerModel struct {
	ID       int    `db:"LOGGER_ID"              json:"loggerId"`
	DeviceID string `db:"DEVICE_ID"              json:"deviceID"`
	Name     string `db:"LOGGER_NAME"            json:"loggerName"`
	Label    string `db:"LOGGER_LABEL"           json:"loggerLabel"`
	Person   string `db:"RESPONSIBLE_PERSON"     json:"responsiblePerson"`
	Station  int    `db:"UNTERSUCHUNGSGEBIET_ID" json:"stationId"`
}

type LoggerData []LoggerModel

func (d *LoggerData) Length() int {
	return len(*d)
}

func (db *DataBase) GetLoggersAll(projectId, start, limit int) (*LoggerData, error) {
	return db.queryLoggers(LOGGERS_QUERY, projectId, start, limit)
}

func (db *DataBase) GetLoggerByID(projectId, loggerId, start, limit int) (*LoggerData, error) {
	loggers, err := db.queryLoggers(
		LOGGERS_BY_LOGGERID_QUERY,
		projectId, loggerId,
		start, limit,
	)

	if (err == nil) && (len(*loggers) == 0) {
		return nil, fmt.Errorf("Failed to retrieve logger id: %v", loggerId)
	}

	return loggers, err
}

func (db *DataBase) queryDeviceID(projectId, loggerId int) (string, error) {
	loggers, err := db.GetLoggerByID(
		projectId, loggerId,
		0, 1,
	)

	if err != nil {
		return "", err
	}

	deviceID := (*loggers)[0].DeviceID
	if deviceID == "" {
		return "", fmt.Errorf("No DEVICE_ID found for logger id: %v", loggerId)
	}

	return deviceID, nil
}

func (db *DataBase) queryLoggers(query string, args ...interface{}) (*LoggerData, error) {

	rows, err := db.Queryx(query, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var out LoggerData
	for rows.Next() {
		rowData := LoggerModel{}
		err := rows.StructScan(&rowData)
		if err != nil {
			return nil, err
		}
		out = append(out, rowData)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return &out, nil
}
