package db

import (
	"database/sql"
	"database/sql/driver"
	s "dmpapi/settings"
	"encoding/json"
	"fmt"
	"strings"
	"time"
)

type DBData interface {
	Length() int
}

type NullFloat64 struct {
	sql.NullFloat64
}

func (v NullFloat64) MarshalJSON() ([]byte, error) {
	if v.Valid {
		return json.Marshal(v.Float64)
	}
	return json.Marshal(nil)
}

func (v *NullFloat64) UnmarshalJSON(data []byte) error {
	// Unmarshalling into a pointer will let us detect null
	var x *float64
	if err := json.Unmarshal(data, &x); err != nil {
		return err
	}
	if x != nil {
		v.Valid = true
		v.Float64 = *x
	} else {
		v.Valid = false
	}
	return nil
}

// NOTE:
// I wonder if embedding would solve the problem of missing
// methods like `Format` and `Equal`
type UploadDate time.Time

func (d *UploadDate) Scan(value interface{}) error {
	t, err := time.Parse(s.DATE_FORMAT, value.(string))
	if err != nil {
		t = time.Time{}
	}
	*d = UploadDate(t)
	return nil
}

func (d *UploadDate) UnmarshalJSON(text []byte) error {
	str := strings.Trim(string(text), "\"")
	t, err := time.Parse(s.DATE_FORMAT, str)
	if err != nil {
		return err
	}
	*d = UploadDate(t)
	return nil
}

func (d UploadDate) MarshalJSON() ([]byte, error) {
	return []byte(`"` + time.Time(d).Format(s.DATE_FORMAT) + `"`), nil
}

func (d UploadDate) Format() string {
	t := time.Time(d)
	return t.Format(s.DATE_FORMAT)
}

func (d UploadDate) Equal(other UploadDate) bool {
	return time.Time(d).Equal(time.Time(other))
}

func (d UploadDate) Before(other UploadDate) bool {
	return time.Time(d).Before(time.Time(other))
}

func (d UploadDate) After(other UploadDate) bool {
	return time.Time(d).After(time.Time(other))
}

// func (d UploadDate) IsZero() bool {
// 	return time.Time(d).IsZero()
// }

type Boolean bool

func (b *Boolean) Scan(value interface{}) error {

	if value == nil {
		value = 0
	}

	res, err := driver.Int32.ConvertValue(value)
	if err != nil {
		return err
	}

	i, ok := res.(int64)
	if !ok {
		return fmt.Errorf("unable to scan value '%v' into field of type bool", res)
	}
	*b = i > 0
	return nil

}
