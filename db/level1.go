package db

import (
	s "dmpapi/settings"
	"fmt"
	"math"
	"time"
)

type L1DataModel struct {
	recordModel
	dataModel
}

type L1Data []L1DataModel

func (d *L1Data) Length() int {
	return len(*d)
}

func (db *DataBase) GetSensorDataLevel1Cached(projectId, sensorId, start, limit int, startDate, endDate time.Time) (*L1Data, error) {
	// join the sensor information manually, to safe database cycles
	sensors, err := db.GetSensorsByID(projectId, sensorId, 0, math.MaxInt64)
	if err != nil {
		return nil, err
	}

	if len(*sensors) == 0 {
		return nil, fmt.Errorf("no such sensor found: %v", sensorId)
	}

	sensor := (*sensors)[0]

	data, err := db.getL1Data(
		L1_CACHE_SENSOR_QUERY,
		sensorId,
		startDate.Format(s.DATE_FORMAT), endDate.Format(s.DATE_FORMAT),
		start, limit,
	)

	if err != nil {
		return data, err
	}

	for i, row := range *data {
		row.SensorName = sensor.Name
		row.SensorLabel = sensor.Label
		(*data)[i] = row
	}

	return data, nil
}

func (db *DataBase) GetSensorDataLevel1(projectId, sensorId, start, limit int, startDate, endDate time.Time) (*L1Data, error) {
	// TODO: Implement the handling of a virtual sensor query parameter -> use the format sql query approach
	return db.getL1Data(
		L1_SENSOR_QUERY,
		projectId, sensorId,
		startDate.Format(s.DATE_FORMAT), endDate.Format(s.DATE_FORMAT),
		start, limit,
	)
}

func (db *DataBase) GetLoggerDataLevel1Cached(projectId, loggerId, start, limit int, startDate, endDate time.Time, includeVirtual bool) (*L1Data, error) {
	// join the sensor information manually, to safe database cycles
	lut, err := db.querySensorInformation(projectId, loggerId)
	if err != nil {
		return nil, err
	}

	var query string
	if includeVirtual {
		query = fmt.Sprintf(L1_CACHE_LOGGER_QUERY, "")
	} else {
		query = fmt.Sprintf(L1_CACHE_LOGGER_QUERY, "AND s.virtual_sensor = 0")
	}

	data, err := db.getL1Data(
		query,
		loggerId,
		startDate.Format(s.DATE_FORMAT), endDate.Format(s.DATE_FORMAT),
		start, limit,
	)

	if err != nil {
		return nil, err
	}

	for i, row := range *data {
		sensor := lut[row.SensorID]
		row.SensorName = sensor.Name
		row.SensorLabel = sensor.Label
		(*data)[i] = row
	}

	return data, nil
}

func (db *DataBase) GetLoggerDataLevel1(projectId, loggerId, start, limit int, startDate, endDate time.Time, virtual bool) (*L1Data, error) {
	return db.getL1Data(
		L1_LOGGER_QUERY,
		projectId, loggerId,
		startDate.Format(s.DATE_FORMAT), endDate.Format(s.DATE_FORMAT),
		start, limit,
	)
}

func (db *DataBase) GetSensorDataLevel1Aggregate(projectId, sensorId, start, limit int, startDate, endDate time.Time, aggregate, function string) (*L1Data, error) {
	query := fmt.Sprintf(L1_AGGREGATE_SENSOR_QUERY, s.AGGREGATION_PERIODS[aggregate], s.AGGREGATION_FUNCTIONS[function])
	return db.getL1Data(
		query,
		projectId, sensorId,
		startDate.Format(s.DATE_FORMAT), endDate.Format(s.DATE_FORMAT),
		start, limit,
	)
}

func (db *DataBase) GetLoggerDataLevel1Aggregate(projectId, loggerId, start, limit int, startDate, endDate time.Time, aggregate, function string) (*L1Data, error) {
	query := fmt.Sprintf(L1_AGGREGATE_LOGGER_QUERY, s.AGGREGATION_PERIODS[aggregate], s.AGGREGATION_FUNCTIONS[function])
	return db.getL1Data(
		query,
		projectId, loggerId,
		startDate.Format(s.DATE_FORMAT), endDate.Format(s.DATE_FORMAT),
		start, limit,
	)
}

func (db *DataBase) getL1Data(query string, args ...interface{}) (*L1Data, error) {

	rows, err := db.Queryx(query, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var out L1Data
	for rows.Next() {
		rowData := L1DataModel{}
		err := rows.StructScan(&rowData)
		if err != nil {
			return nil, err
		}
		out = append(out, rowData)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return &out, nil
}
