package db

import (
	"fmt"
	_ "github.com/godror/godror"
	"github.com/jmoiron/sqlx"
	"os"
)

type DataBase struct {
	*sqlx.DB
}

func (db *DataBase) Connect(username string, password string) error {

	_username := os.Getenv("APIUSER")
	_password := os.Getenv("APIPASS")
	template := "user=%s password=%s connectString=%s standaloneConnection=true"

	if (username != _username) || (password != _password) {
		// check credentials
		connection, err := sqlx.Open("godror", fmt.Sprintf(template, username, password, "LOGGER"))
		if err != nil {
			connection.Close()
			return err
		}
		db.DB = connection

		// just to check user credentials
		if err := db.Ping(); err != nil {
			db.Close()
			return err
		}
		db.Close()
	}

	connection, err := sqlx.Open("godror", fmt.Sprintf(template, _username, _password, "LOGGER"))
	if err != nil {
		connection.Close()
		return err
	}
	db.DB = connection
	if err := db.Ping(); err != nil {
		db.Close()
		return err
	}

	return nil
}
