package settings

import (
	"time"
)

type key int

const (
	SUPER_USER = "DMP_SAQC"

	PAGE_LIMIT = 1000000

	DATE_FORMAT = "2006-01-02 15:04:05"

	LEVEL1_URL  = "https://logger-worker.intranet.ufz.de/gateway/upload/device/"
	LEVEL2A_URL = "https://logger-worker.intranet.ufz.de/gateway/uploadl2a/device/"

	PARAMETER_KEY key = iota
)

var (
	AGGREGATION_PERIODS = map[string]string{
		"daily":   "DD",
		"monthly": "MM",
		"yearly":  "YY",
		"hourly":  "HH",
	}

	AGGREGATION_FUNCTIONS = map[string]string{
		"sum":  "SUM",
		"mean": "AVG",
	}

	MIN_DATE, _  = time.Parse(DATE_FORMAT, "1900-01-01 00:00:00")
	MAX_DATE, _  = time.Parse(DATE_FORMAT, "2100-12-31 23:59:59")
	DATE_LAYOUTS = []string{
		// TODO:
		// provide feedback when date parsing fails
		// currently an empty result set is provided
		// -> not too helpfull
		"2006-01-02T15:04:05.999999999Z",
		"2006-01-02T15:04:05.999999999",
		"2006-01-02",
		DATE_FORMAT,
	}
)
