package main

import (
	"dmpapi/controller"
	"dmpapi/db"
	s "dmpapi/settings"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"net/http"
	"os"
	"strings"
)

var (
	logLevels = map[string]zerolog.Level{
		"DEBUG": zerolog.DebugLevel,
		"INFO":  zerolog.InfoLevel,
	}
)

func initRouter(db *db.DataBase) *mux.Router {

	router := mux.NewRouter().PathPrefix("/dmpapi/v1/projects/{projectId:[0-9]+}").Subrouter()
	// add middleware
	router.Use(loggingMiddleware)
	router.Use(contentMiddleware)
	router.Use(parameterMiddleware)

	// init controllers
	loggers := controller.InitLoggers(db)
	sensors := controller.InitSensors(db)
	l1data := controller.InitL1Data(db)
	l2data := controller.InitL2Data(db)

	// add routes
	router.HandleFunc("/loggers", getWrapper(db, loggers.GetAll)).Methods("GET")
	router.HandleFunc("/loggers/{loggerId:[0-9]+}", getWrapper(db, loggers.GetID)).Methods("GET")
	router.HandleFunc("/loggers/{loggerId:[0-9]+}/sensors", getWrapper(db, loggers.GetSensors)).Methods("GET")
	router.HandleFunc("/loggers/{loggerId:[0-9]+}/data/level1", getWrapper(db, l1data.GetLogger)).Methods("GET")
	router.HandleFunc("/loggers/{loggerId:[0-9]+}/data/level2", getWrapper(db, l2data.GetLogger)).Methods("GET")
	router.HandleFunc("/loggers/{loggerId:[0-9]+}/cache/level1", getWrapper(db, l1data.GetLoggerCached)).Methods("GET")
	router.HandleFunc("/loggers/{loggerId:[0-9]+}/cache/level2", getWrapper(db, l2data.GetLoggerCached)).Methods("GET")
	router.HandleFunc("/loggers/{loggerId:[0-9]+}/data/level1", postWrapper(db, l1data.PostLogger)).Methods("POST")
	router.HandleFunc("/loggers/{loggerId:[0-9]+}/data/level2", postWrapper(db, l2data.PostLogger)).Methods("POST")
	router.HandleFunc(
		"/loggers/{loggerId:[0-9]+}/data/level1/{period:(?:hourly|daily|monthly|yearly)}/{func:(?:sum|mean)}",
		getWrapper(db, l1data.GetLoggerAggregate)).Methods("GET")
	router.HandleFunc(
		"/loggers/{loggerId:[0-9]+}/data/level2/{period:(?:hourly|daily|monthly|yearly)}/{func:(?:sum|mean)}",
		getWrapper(db, l2data.GetLoggerAggregate)).Methods("GET")

	router.HandleFunc("/sensors", getWrapper(db, sensors.GetAll)).Methods("GET")
	router.HandleFunc("/sensors/{sensorId:[0-9]+}", getWrapper(db, sensors.GetID)).Methods("GET")
	router.HandleFunc("/sensors/{sensorName:\\w+}", getWrapper(db, sensors.GetName)).Methods("GET")

	router.HandleFunc("/sensors/{sensorId:[0-9]+}/data/level1", getWrapper(db, l1data.GetSensor)).Methods("GET")
	router.HandleFunc("/sensors/{sensorId:[0-9]+}/data/level2", getWrapper(db, l2data.GetSensor)).Methods("GET")
	router.HandleFunc("/sensors/{sensorId:[0-9]+}/cache/level1", getWrapper(db, l1data.GetSensorCached)).Methods("GET")
	router.HandleFunc("/sensors/{sensorId:[0-9]+}/cache/level2", getWrapper(db, l2data.GetSensorCached)).Methods("GET")

	router.HandleFunc(
		"/sensors/{sensorId:[0-9]+}/data/level1/{period:(?:hourly|daily|monthly|yearly)}/{func:(?:sum|mean)}",
		getWrapper(db, l1data.GetSensorAggregate)).Methods("GET")
	router.HandleFunc(
		"/sensors/{sensorId:[0-9]+}/data/level2/{period:(?:hourly|daily|monthly|yearly)}/{func:(?:sum|mean)}",
		getWrapper(db, l2data.GetSensorAggregate)).Methods("GET")

	return router
}

func initLogging(logging string) {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: s.DATE_FORMAT})

	level, ok := logLevels[strings.ToUpper(logging)]
	if !ok {
		level = logLevels["INFO"]
	}
	zerolog.SetGlobalLevel(level)
}

func main() {

	logging := os.Getenv("LOGGING")
	initLogging(logging)

	db := &db.DataBase{}

	router := initRouter(db)
	log.Info().Msg("Up and running")
	err := http.ListenAndServe(":8080", router)
	if err != nil {
		log.Fatal().Err(err)
	}
}
