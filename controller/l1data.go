package controller

import (
	"dmpapi/db"
	t "dmpapi/transport"
	"net/http"
)

type L1Data struct {
	*db.DataBase
}

func InitL1Data(db *db.DataBase) L1Data {
	return L1Data{db}
}

func (d L1Data) GetSensor(p t.QueryParam) (db.DBData, error) {
	// TODO: rename
	return d.GetSensorDataLevel1(
		p.ProjectID, p.SensorID, p.Start, p.Limit, p.StartDate, p.EndDate,
	)
}

func (d L1Data) GetSensorCached(p t.QueryParam) (db.DBData, error) {
	// TODO: Rename to GetSensor
	return d.GetSensorDataLevel1Cached(
		p.ProjectID, p.SensorID, p.Start, p.Limit, p.StartDate, p.EndDate,
	)
}

func (d L1Data) GetSensorAggregate(p t.QueryParam) (db.DBData, error) {
	return d.GetSensorDataLevel1Aggregate(
		p.ProjectID, p.SensorID,
		p.Start, p.Limit,
		p.StartDate, p.EndDate,
		p.AggregationPeriod, p.AggregationFunc,
	)
}

func (d L1Data) GetLoggerCached(p t.QueryParam) (db.DBData, error) {
	// TODO: rename to GetLogger
	return d.GetLoggerDataLevel1Cached(
		p.ProjectID, p.LoggerID, p.Start, p.Limit, p.StartDate, p.EndDate, p.IncludeVirtual,
	)
}

func (d L1Data) GetLogger(p t.QueryParam) (db.DBData, error) {
	// TODO: remove function
	return d.GetLoggerDataLevel1(
		p.ProjectID, p.LoggerID, p.Start, p.Limit, p.StartDate, p.EndDate, p.IncludeVirtual,
	)
}

func (d L1Data) GetLoggerAggregate(p t.QueryParam) (db.DBData, error) {
	return d.GetLoggerDataLevel1Aggregate(
		p.ProjectID, p.LoggerID,
		p.Start, p.Limit,
		p.StartDate, p.EndDate,
		p.AggregationPeriod, p.AggregationFunc,
	)
}

func (d L1Data) PostLogger(w http.ResponseWriter, r *http.Request, p t.QueryParam) (*http.Request, error) {
	return d.PostLevel1Data(t.InitBodyDecoder(w, r), p.ProjectID, p.LoggerID)
}
