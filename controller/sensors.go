package controller

import (
	"dmpapi/db"
	t "dmpapi/transport"
)

type Sensors struct {
	*db.DataBase
}

func InitSensors(db *db.DataBase) Sensors {
	return Sensors{db}
}

func (s Sensors) GetID(p t.QueryParam) (db.DBData, error) {
	return s.GetSensorsByID(p.ProjectID, p.SensorID, p.Start, p.Limit)
}

func (s Sensors) GetName(p t.QueryParam) (db.DBData, error) {
	return s.GetSensorsByName(p.ProjectID, p.SensorName, p.Start, p.Limit)
}

func (s Sensors) GetAll(p t.QueryParam) (db.DBData, error) {
	return s.GetSensorsAll(p.ProjectID, p.Start, p.Limit)
}
