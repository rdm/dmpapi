package controller

import (
	"dmpapi/db"
	t "dmpapi/transport"
)

type Loggers struct {
	*db.DataBase
}

func InitLoggers(db *db.DataBase) Loggers {
	return Loggers{db}
}

func (l Loggers) GetAll(p t.QueryParam) (db.DBData, error) {
	return l.GetLoggersAll(p.ProjectID, p.Start, p.Limit)
}

func (l Loggers) GetID(p t.QueryParam) (db.DBData, error) {
	return l.GetLoggerByID(p.ProjectID, p.LoggerID, 0, 1)
}

func (l Loggers) GetSensors(p t.QueryParam) (db.DBData, error) {
	return l.GetSensorsByLoggerID(p.ProjectID, p.LoggerID, p.Start, p.Limit)
}
