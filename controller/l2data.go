package controller

import (
	"dmpapi/db"
	t "dmpapi/transport"
	"net/http"
)

type L2Data struct {
	*db.DataBase
}

func InitL2Data(db *db.DataBase) L2Data {
	return L2Data{db}
}

func (d L2Data) GetSensorCached(p t.QueryParam) (db.DBData, error) {
	return d.GetSensorDataLevel2Cached(p.ProjectID, p.SensorID, p.Start, p.Limit, p.StartDate, p.EndDate)
}

func (d L2Data) GetSensor(p t.QueryParam) (db.DBData, error) {
	return d.GetSensorDataLevel2(p.ProjectID, p.SensorID, p.Start, p.Limit, p.StartDate, p.EndDate)
}

func (d L2Data) GetSensorAggregate(p t.QueryParam) (db.DBData, error) {
	return d.GetSensorDataLevel2Aggregate(
		p.ProjectID, p.SensorID,
		p.Start, p.Limit,
		p.StartDate, p.EndDate,
		p.BadMax, p.DoubtMax,
		p.AggregationPeriod, p.AggregationFunc,
	)
}

func (d L2Data) GetLogger(p t.QueryParam) (db.DBData, error) {
	return d.GetLoggerDataLevel2(
		p.ProjectID, p.LoggerID, p.Start, p.Limit, p.StartDate, p.EndDate,
	)
}

func (d L2Data) GetLoggerCached(p t.QueryParam) (db.DBData, error) {
	return d.GetLoggerDataLevel2Cached(
		p.ProjectID, p.LoggerID, p.Start, p.Limit, p.StartDate, p.EndDate,
	)
}

func (d L2Data) GetLoggerAggregate(p t.QueryParam) (db.DBData, error) {
	return d.GetLoggerDataLevel2Aggregate(
		p.ProjectID, p.LoggerID,
		p.Start, p.Limit,
		p.StartDate, p.EndDate,
		p.BadMax, p.DoubtMax,
		p.AggregationPeriod, p.AggregationFunc,
	)
}

func (d L2Data) PostLogger(w http.ResponseWriter, r *http.Request, p t.QueryParam) (*http.Request, error) {
	return d.PostLevel2Data(t.InitBodyDecoder(w, r), p.ProjectID, p.LoggerID)
}
