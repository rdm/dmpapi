package transport

import (
	"net/url"
	"testing"
)

func TestLastURL(t *testing.T) {

	u, _ := url.Parse("http://www.testing.com")
	tables := []struct {
		start    int
		limit    int
		expected string
	}{
		{0, 10, ""},
		{2, 10, "http://www.testing.com?limit=2&start=0"},
		{5, 10, "http://www.testing.com?limit=5&start=0"},
		{40, 25, "http://www.testing.com?limit=25&start=15"},
	}
	for _, table := range tables {
		got := lastURL(u, table.start, table.limit)
		if got.String() != table.expected {
			t.Errorf("expected '%v' got '%v'", table.expected, got)
		}
	}
}

func TestNextURL(t *testing.T) {

	u, _ := url.Parse("http://www.testing.com")
	tables := []struct {
		start    int
		limit    int
		length   int
		expected string
	}{
		{2, 10, 100, "http://www.testing.com?limit=10&start=12"},
		{0, 100, 100, "http://www.testing.com?limit=100&start=100"},
		{5, 15, 5, ""},
	}
	for _, table := range tables {
		got := nextURL(u, table.length, table.start, table.limit)
		if got.String() != table.expected {
			t.Errorf("expected '%v' got '%v'", table.expected, got)
		}
	}
}

func TestThisURL(t *testing.T) {
	u, _ := url.Parse("http://www.testing.com")
	expected := "http://www.testing.com?limit=100&start=20"
	got := thisURL(u, 20, 100)
	if got.String() != expected {
		t.Errorf("expected '%v' got '%v'", expected, got)
	}
}
