package transport

import (
	"encoding/json"
	"github.com/rs/zerolog/log"
	"net/http"
)

type Envelope struct {
	Pagination map[string]string `json:"pagination"`
	Data       interface{}       `json:"data"`
	Error      string            `json:"error"`
}

func (e *Envelope) Encode(w http.ResponseWriter) {
	enc := json.NewEncoder(w)
	enc.SetEscapeHTML(false)
	// TODO: handle error
	err := enc.Encode(e)
	if err != nil {
		log.Debug().Msgf("Envelope: %v", err)
	}
}

func ErrorResponse(w http.ResponseWriter, err error, code int) {
	http.Error(w, "", code)
	out := &Envelope{
		Pagination: nil,
		Data:       nil,
		Error:      err.Error(),
	}
	out.Encode(w)
}

func GetResponse(w http.ResponseWriter, paging map[string]string, data interface{}) {
	// NOTE:
	// rearange so that we first decode everything, check for errors
	// add potential encoding errors to the output
	out := Envelope{
		Pagination: paging,
		Data:       data,
		Error:      "",
	}
	out.Encode(w)
}

func PostResponse(w http.ResponseWriter) {
	w.WriteHeader(http.StatusOK)
}
