package transport

import (
	s "dmpapi/settings"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"net/http"
	"net/url"
	"reflect"
	"strconv"
	"time"
)

var decoder = initDecoder()

func initDecoder() *schema.Decoder {
	dec := schema.NewDecoder()
	var dateConverter = func(date string) reflect.Value {
		for _, layout := range s.DATE_LAYOUTS {
			val, err := time.Parse(layout, date)
			if err == nil {
				return reflect.ValueOf(val)
			}
		}
		return reflect.Value{}
	}

	dec.RegisterConverter(time.Time{}, dateConverter)
	// dec.IgnoreUnknownKeys(true)
	return dec
}

type QueryParam struct {
	ProjectID         int       `schema:"projectId"`
	LoggerID          int       `schema:"loggerId"`
	SensorID          int       `schema:"sensorId"`
	SensorName        string    `schema:"sensorName"`
	IncludeVirtual    bool      `schema:"includeVirtual"`
	Start             int       `schema:"start"`
	Limit             int       `schema:"limit"`
	StartDate         time.Time `schema:"startDate"`
	EndDate           time.Time `schema:"endDate"`
	BadMax            float64   `schema:"badMax"`
	DoubtMax          float64   `schema:"doubtMax"`
	AggregationFunc   string    `schema:"func"`
	AggregationPeriod string    `schema:"period"`
}

func InitParams(r *http.Request) (QueryParam, error) {

	params := initRequestParams(r)

	p := QueryParam{
		ProjectID:         -1,
		LoggerID:          -1,
		SensorID:          -1,
		SensorName:        "",
		IncludeVirtual:    true,
		Start:             -1,
		Limit:             -1,
		BadMax:            0.5,
		DoubtMax:          0.5,
		AggregationFunc:   "mean",
		AggregationPeriod: "daily",
	}

	if err := decoder.Decode(&p, params); err != nil {
		return QueryParam{}, err
	}

	if p.Start < 0 {
		p.Start = 0
	}

	if p.Limit < 0 || p.Limit > s.PAGE_LIMIT {
		p.Limit = s.PAGE_LIMIT
	}

	if p.StartDate.Before(s.MIN_DATE) {
		p.StartDate = s.MIN_DATE
	}

	if p.EndDate.Before(s.MIN_DATE) || p.EndDate.After(s.MAX_DATE) {
		p.EndDate = s.MAX_DATE
	}

	if p.StartDate.Before(s.MIN_DATE) || p.EndDate.Before(s.MIN_DATE) || p.EndDate.After(s.MAX_DATE) || p.StartDate.After(s.MAX_DATE) || p.EndDate.Before(s.MIN_DATE) {
		return QueryParam{}, fmt.Errorf("invalid startDate and endDate combination %v - %v", p.StartDate, p.EndDate)
	}

	if p.BadMax < 0 || p.BadMax > 1 {
		return QueryParam{}, fmt.Errorf("badMax is expected to be in [0, 1], got: %v", p.BadMax)
	}

	if p.DoubtMax < 0 || p.DoubtMax > 1 {
		return QueryParam{}, fmt.Errorf("doubtMax is expected to be in [0, 1], got: %v", p.DoubtMax)
	}

	if _, ok := s.AGGREGATION_PERIODS[p.AggregationPeriod]; !ok {
		return QueryParam{}, fmt.Errorf("invalid aggregation period: %v", p.AggregationPeriod)
	}

	if _, ok := s.AGGREGATION_FUNCTIONS[p.AggregationFunc]; !ok {
		return QueryParam{}, fmt.Errorf("invalid aggregation function: %v", p.AggregationFunc)
	}

	return p, nil

}

func (p QueryParam) GetPages(u *url.URL, length int) map[string]string {
	out := map[string]string{
		"last": lastURL(u, p.Start, p.Limit).String(),
		"next": nextURL(u, length, p.Start, p.Limit).String(),
		"self": thisURL(u, p.Start, p.Limit).String(),
	}
	return out
}

func initRequestParams(r *http.Request) map[string][]string {
	// collect all parameters (path and query)
	out := map[string][]string{}
	for k, v := range mux.Vars(r) {
		out[k] = []string{v}
	}
	for k, v := range r.URL.Query() {
		out[k] = v
	}
	return out
}

func thisURL(u *url.URL, start int, limit int) *url.URL {
	return addQueryParameters(
		u, map[string]string{
			"start": strconv.Itoa(start),
			"limit": strconv.Itoa(limit),
		},
	)
}

func lastURL(u *url.URL, start int, limit int) *url.URL {

	start = start - limit
	if start < 0 {
		limit = limit + start
		start = 0
	}
	if limit <= 0 {
		return &url.URL{}
	}
	return addQueryParameters(
		u, map[string]string{
			"start": strconv.Itoa(start),
			"limit": strconv.Itoa(limit),
		},
	)
}

func nextURL(u *url.URL, length int, start int, limit int) *url.URL {

	if length < limit {
		return &url.URL{}
	}

	return addQueryParameters(
		u, map[string]string{
			"start": strconv.Itoa(start + limit),
			"limit": strconv.Itoa(limit),
		},
	)
}

func addQueryParameters(u *url.URL, params map[string]string) *url.URL {
	query := url.Values{}
	for key, value := range params {
		query.Set(key, value)
	}
	u.RawQuery = query.Encode()
	return u
}
