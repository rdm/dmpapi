package transport

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/url"
	"time"
)

func InitBodyDecoder(w http.ResponseWriter, r *http.Request) *json.Decoder {
	// TODO:
	//     - add consistency checks
	//     - add date parser from parameter.go

	r.Body = http.MaxBytesReader(w, r.Body, 10<<20)

	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()

	// TODO: some sort of safety nets
	//       (empty body, more than PAGE_LIMIT lines)
	// log.Println("decodeBody: ", length, PutData)
	return dec
}

func MakeRequest(req *http.Request) error {

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	if res.StatusCode < 200 || res.StatusCode > 299 {
		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return err
		}
		return fmt.Errorf("Upload failed with code %v: %v", res.StatusCode, string(body))
	}
	return nil
}

func FileRequest(u *url.URL, content []byte) (*http.Request, error) {
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	filename := fmt.Sprintf("file-%d", time.Now().Unix())
	part, err := writer.CreateFormFile("file", filename)
	if err != nil {
		return nil, err
	}

	if _, err := part.Write(content); err != nil {
		return nil, err
	}

	if writer.Close() != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", u.String(), body)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", writer.FormDataContentType())
	return req, nil
}
